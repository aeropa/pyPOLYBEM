# pyPOLYBEM

## Description
pyPOLYBEM is a software library for modeling polycrystalline materials at their grain scale.

## Graphical Abstract
![alt text](./Ad/GraphicalAbstract.jpeg)

## Installation
Routines may be provided upon request contacting [Ivano Benedetti](mailto:ivano.benedetti@unipa.it) or [Vincenzo Gulizzi](mailto:vincenzo.gulizzi@unipa.it).

## Authors and acknowledgment
Main developers: [Ivano Benedetti](mailto:ivano.benedetti@unipa.it), [Vincenzo Gulizzi](mailto:vincenzo.gulizzi@unipa.it)

We acknowledge Chris Rycroft and Romain Quey for their support in the use of [VORO++](https://math.lbl.gov/voro++/) and [Neper](https://neper.info), respectively, software libraries that we use to generate polycrystalline microstructures based on the Voronoi tessellation algorithm.

## License
TBA.

## Project status
In progress.

## Applications
Below the list of Journal and Conference papers on pyPOLYBEM.
### Journal papers
* Benedetti, I. (2023). An integral framework for computational thermo-elastic homogenization of polycrystalline materials. _Computer Methods in Applied Mechanics and Engineering_, 407, 115927. [doi](https://doi.org/10.1016/j.cma.2023.115927)
* Parrinello, F., & Benedetti, I. (2022). A coupled plasticity-damage cohesive-frictional interface for low-cycle fatigue analysis. _International Journal of Mechanical Sciences_, 224, 107298. [doi](https://doi.org/10.1016/j.ijmecsci.2022.107298)
* Parrinello, F., Gulizzi, V., & Benedetti, I. (2021). A computational framework for low-cycle fatigue in polycrystalline materials. _Computer Methods in Applied Mechanics and Engineering_, 383, 113898. [doi](https://doi.org/10.1016/j.cma.2021.113898)
* Benedetti, I., & Gulizzi, V. (2018). A grain-scale model for high-cycle fatigue degradation in polycrystalline materials. _International Journal of Fatigue_, 116, 90-105. [doi](https://doi.org/10.1016/j.ijfatigue.2018.06.010)
* Benedetti, I., Gulizzi, V., & Milazzo, A. (2018). Grain-boundary modelling of hydrogen assisted intergranular stress corrosion cracking. _Mechanics of Materials_, 117, 137-151. [doi](https://doi.org/10.1016/j.mechmat.2017.11.001)
* Gulizzi, V., Rycroft, C. H., & Benedetti, I. (2018). Modelling intergranular and transgranular micro-cracking in polycrystalline materials. _Computer Methods in Applied Mechanics and Engineering_, 329, 168-194. [doi](https://doi.org/10.1016/j.cma.2017.10.005)
* Benedetti, I., Gulizzi, V., & Mallardo, V. (2016). A grain boundary formulation for crystal plasticity. _International Journal of Plasticity_, 83, 202-224. [doi](https://doi.org/10.1016/j.ijplas.2016.04.010)
* Gulizzi, V., & Benedetti, I. (2016). Micro-cracking of brittle polycrystalline materials with initial damage. _European Journal of Computational Mechanics_, 25(1-2), 38-53. [doi](https://doi.org/10.1080/17797179.2016.1181032)
* Gulizzi, V., Milazzo, A., & Benedetti, I. (2015). An enhanced grain-boundary framework for computational homogenization and micro-cracking simulations of polycrystalline materials. _Computational Mechanics_, 56(4), 631-651. [doi](https://doi.org/10.1007/s00466-015-1192-8)
### Conference papers
* Gulizzi, V., Milazzo, A., & Benedetti, I. (2016). A micro-mechanical model for grain-boundary cavitation in polycrystalline materials. _Key Engineering Materials_, 665, 65-68. [doi](https://doi.org/10.4028/www.scientific.net/KEM.665.65)
